﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
using System.Threading.Tasks;

public class GameRules : MonoBehaviour
{
    // 2 joueurs poussent un ballon
    // si le ballon touche un but
    // => score pour l'équipe qui marque (équpe adverse de celle qui prend le but)
    // => on réenge: remise à la position initiale des joueurs et de la balle

    [Serializable]
    public struct Entity
    {
        public GameObject gameObject;
        [HideInInspector]
        public Vector3 initialPosition;
    }

    [Serializable]
    public struct Team
    {
        public GameObject goal;
        public Text score;
    }

    public Entity[] players;
    public Entity ball;

    public Entity spikes;
    public Team orange;
    public Team blue;

    public GameObject GoalFX;
    public GameObject ExplosionFX;

    public Text OrangeRoundCounter;
    public Text BlueRoundCounter;
    private int roundNumber = 1;

    public Text BlueRounds;
    private int blueRoundsNumber = 0;
    public Text OrangeRounds;
    private int orangeRoundsNumber = 0;

    private int orangeScore = 0;
    private int blueScore = 0;

    void Start()
    {

        RecordInitialPositions();

        BallCollisionEmitter emitter = ball.gameObject.GetComponentInChildren<BallCollisionEmitter>();
        emitter.OnCollided += BallCollided;

        SpikesCollisionEmitter spikesEmitter = spikes.gameObject.GetComponentInChildren<SpikesCollisionEmitter>();
        spikesEmitter.OnCollided += SpikesCollided;

        StartRound();
    }

    private void StartRound()
    {

        UpdateScores();

        this.BlueRoundCounter.text = $"Round {roundNumber}";
        this.OrangeRoundCounter.text = $"Round {roundNumber}";
        this.BlueRounds.text = $"Rounds : {blueRoundsNumber}";
        this.OrangeRounds.text = $"Round : {orangeRoundsNumber}";
        StartCoroutine(StartFading());
    }

    public float fadeDuration = 3.0f;

    private IEnumerator StartFading()
    {
        yield return StartCoroutine(Fade(0.0f, 1.0f, fadeDuration));
        yield return StartCoroutine(Fade(1.0f, 0.0f, fadeDuration));
    }

    private IEnumerator Fade(float startLevel, float endLevel, float time)
    {
        float speed = 1.0f / time;

        for (float t = 0.0f; t < 1.0; t += Time.deltaTime * speed)
        {
            float a = Mathf.Lerp(startLevel, endLevel, t);
            this.OrangeRoundCounter.color = new Color(this.OrangeRoundCounter.color.r,
                                                    this.OrangeRoundCounter.color.g,
                                                    this.OrangeRoundCounter.color.b, a);

            this.BlueRoundCounter.color = new Color(this.BlueRoundCounter.color.r,
            this.BlueRoundCounter.color.g,
            this.BlueRoundCounter.color.b, a);
            yield return 0;
        }
    }

    void Destroy()
    {
        BallCollisionEmitter emitter = ball.gameObject.GetComponentInChildren<BallCollisionEmitter>();
        emitter.OnCollided -= BallCollided;
    }

    private void BallCollided(Collision2D collision)
    {
        if (collision.collider.gameObject.name == orange.goal.name)
        {
            ShowFireworks(collision);
            blueScore++;
            UpdateScores();
            ResetPositions();
        }
        else if (collision.collider.gameObject.name == blue.goal.name)
        {
            ShowFireworks(collision);
            orangeScore++;
            UpdateScores();
            ResetPositions();
        }

    }

    private void ResetPositions()
    {
        for (int i = 0; i < players.Length; i++)
        {
            players[i].gameObject.transform.position = players[i].initialPosition;
        }
        ball.gameObject.transform.position = ball.initialPosition;
        ball.gameObject.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
    }

    public void RecordInitialPositions()
    {
        // On mémorise les positions initiales
        for (int i = 0; i < players.Length; i++)
        {
            players[i].initialPosition = players[i].gameObject.transform.position;
        }
        ball.initialPosition = ball.gameObject.transform.position;
    }

    public void UpdateScores()
    {
        orange.score.text = orangeScore.ToString();
        blue.score.text = blueScore.ToString();

        CheckForWinner();
    }

    private void CheckForWinner()
    {

        var roundFinished = false;

        if (orangeScore == 3)
        {
            orangeRoundsNumber++;
            roundFinished = true;
        }

        if (blueScore == 3)
        {
            blueRoundsNumber++;
            roundFinished = true;
        }

        if (roundFinished)
        {
            blueScore = 0;
            orangeScore = 0;
            roundNumber++;
            StartRound();
        }
    }

    private void ShowFireworks(Collision2D collision)
    {
        Instantiate(GoalFX, collision.GetContact(0).point, Quaternion.identity);
    }

    private void SpikesCollided(Collision2D collision)
    {
        for (int i = 0; i < players.Length; i++)
        {
            if (collision.gameObject.name == players[i].gameObject.name)
            {
                Instantiate(ExplosionFX, collision.GetContact(0).point, Quaternion.identity);
                players[i].gameObject.transform.position = players[i].initialPosition;
            }
        }
    }
}
