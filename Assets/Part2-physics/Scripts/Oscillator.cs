﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Oscillator : MonoBehaviour
{

    public float speed = 10;
    public float width = 10;
    public float height = 10;
    private float timeCounter = 0;
    private float velocity;
    private Vector3 previousPos;

    private Rigidbody2D body;

    void Start()
    {
        body = GetComponent<Rigidbody2D>();

    }

    // Update is called once per frame
    void Update()
    {

        velocity = ((transform.position - previousPos).magnitude) / Time.deltaTime;
        previousPos = transform.position;
        
        timeCounter += Time.deltaTime * speed;

        float x = Mathf.Cos(timeCounter) * width;
        float y = Mathf.Sin(timeCounter) * height;

        body.velocity = Vector2.zero * velocity;
        body.position = new Vector2(x, y);
    }


}
